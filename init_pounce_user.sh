#!/bin/sh
pouncedir="/var/lib/pounce"
uid="$1"
userid="$(id -u $uid)"

init_directory() {
        previous_uidnumber="$(cat /var/lib/pounce/uidnumber)"
        uidnumber="$(expr $previous_uidnumber + 1)"
        sed -e "s/%UID%/$uid/" -e "s/%UIDNUMBER%/$uidnumber/" /opt/libertacasa/template.ldif | ldapmodify -D 'cn=pounce_adm,ou=syscid-system,dc=syscid,dc=com' -H 'ldaps://gaia.syscid.com/' -xy %%$AUTHSEC%% -v
        status_dir=$?
        if [ "$status_dir" = "0" ]; then
                echo -n "$uidnumber" > /var/lib/pounce/uidnumber
        fi
}

init_local() {
                mkdir $pouncedir/users/$uid
                mkdir $pouncedir/users/$uid/enabled
                mkdir $pouncedir/users/$uid/disabled
                mkdir $pouncedir/users/$uid/certs
                chown -R $userid:pounce $pouncedir/users/$uid
                chmod -R 700 $pouncedir/users/$uid
                /usr/local/bin/pounce -g $pouncedir/users/$uid/ca.pem
                chown $userid:pounce $pouncedir/users/$uid/ca.pem
                chmod 600 $pouncedir/users/$uid/ca.pem
                #mkhomedir_helper $uid
                mkdir -p /home/$uid/.config/systemd/user
                cat <<SERVICE >/home/$uid/.config/systemd/user/pounce@.service
[Unit]
Description=pounce - $uid - %i
Wants=network.target

[Service]
ExecStart=/usr/local/bin/pounce /var/lib/pounce/users/$uid/enabled/%i
Restart=always
RestartSec=15
SyslogIdentifier=pounce-$uid-%i

[Install]
WantedBy=multi-user.target
SERVICE
                chown -R $userid:2000 /home/$uid/
                chmod 700 /home/$uid
}

if [ $uid ]; then
        if [ $userid ]; then
                init_local
        else
                echo "User not found. Attempting to promote POSIX access ..."
                init_directory
                if [ "$status_dir" = "0" ]; then
                        init_local
                else
                        echo "POSIX promiotion failed. User might not exist."
                fi

        fi
else
        echo "Missing username."                                                                                                              
        exit 1                                                                                                                                
fi
